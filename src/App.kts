import kotlin.random.Random

fun fillWarehouse()
{
    println("Fill Warehouse")
    for (i in 0..5)
    {
        warehouse.fillWarehouse(randomName(), Random.nextDouble(0.0, 100.0), "Test")
    }
    warehouse.fillWarehouse("Test", 1.0, "Test")
}


fun displayWarehouse()
{
    println("*** Verfügbare Produkte ***")
    println(warehouse.listOfProductsToString())
}


fun displayShoppingCart() {
    println("*** Einkaufswagen ***")
    println(shoppingCart.listOfAllProducts)
}


fun randomName(maxLength: Int = 10) : String
{
    val charPool : List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')
    val stringLenght = Random.nextInt(0, maxLength)

    return (1..stringLenght)
            .map { Random.nextInt(0, charPool.size) }
            .map(charPool::get)
            .joinToString("")
}


fun clearShoppingCart()
{
    shoppingCart.clear()
    println("Einkaufswagen ist jetzt  Leer")
}


fun buyEverything()
{
    val amount = shoppingCart.buyEverything()
    println("Es wurden $amount Artikel gekauft")
}


fun printHelp()
{
    println("Folge Aktionen sind möglich:")
    println("A=Hinzufügen, K=Alles Kaufen, I=Info, Z=Einkaufswagen, L=Einkaufswagen leeren, E=Exit")
}


fun addProductToCart()
{
    print("Bitte geben Sie den Namen an: ")
    val name : String = readLine() ?: "-"
    print("Bitte geben Sie die Menge an: ")
    val amount : Int = readLine()?.toInt() ?: 1

    shoppingCart.addProductByName(name, amount)
    println("Artikel hinzugefügt")
}


println("Init Warehouse")
var warehouse = de.main.Warehouse()
fillWarehouse()

var shoppingCart = de.main.ShoppingCart(warehouse)

printHelp()

loop@ while (true)
{
    print("*******************************************")
    print("\nBitte geben Sie die gewünschte Aktion an: ")
    val input : String = readLine() ?: "-"

    when (input) {
        "E" -> break@loop
        "L" -> clearShoppingCart()
        "Z" -> displayShoppingCart()
        "I" -> displayWarehouse()
        "K" -> buyEverything()
        "A" -> addProductToCart()
        "H" -> printHelp()
        else -> print("Dies ist keine gültige Option")
    }
}

print("Vielen Dank für ihren Einkauf")
