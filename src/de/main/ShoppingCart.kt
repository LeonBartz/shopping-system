package de.main

import java.time.temporal.TemporalAmount

class ShoppingCart (private val warehouse : Warehouse){
    private val productAndQuantityList = mutableListOf<ShoppingCartEntry>()

    val allProductsAvailable : Boolean
        get() {
            for (entry in productAndQuantityList)
            {
                if(!entry.productAvailable()) return false
            }
            return true
        }

    val totablPrice : Double
        get() {
            var total = 0.0
            for(entry in productAndQuantityList)
            {
                total += entry.price()
            }
            return total
        }

    val listOfAllProducts : String
        get() {
            var list = ""
            for (entry in productAndQuantityList)
            {
                list += "${entry.product.name} x ${entry.amount} Gesamtpreis: ${entry.total().round(2)}\n"
            }
            list += "\nGesamtpreis für alle Produkte: $totablPrice\n"
            if(allProductsAvailable)
            {
                list += "Es sind alle Produkte verfügbar"
            }
            else
            {
                list += "Es sind nicht alle  Produkte verfügbar"
            }
            return list
        }

    fun clear() {
        productAndQuantityList.clear()
    }

    fun buyEverything() : Int
    {
        var bought = 0
        for (entry in productAndQuantityList)
        {
            bought += entry.product.takeItems(entry.product.availableItems())
        }
        return bought
    }

    fun addProductByName(productName : String, amount: Int) : Boolean
    {
        val product = warehouse.getProductByName(productName)
        println(product)
        if(product == null) return false

        val newEntry = ShoppingCartEntry(product, amount)
        return productAndQuantityList.add(newEntry)
    }

    fun Double.round(decimals: Int): Double {
        var multiplier = 1.0
        repeat(decimals) { multiplier *= 10 }
        return kotlin.math.round(this * multiplier) / multiplier
    }

}
