package de.main

class StockUnit(quantity : Int) {
    var quantity : Int = quantity
        get() = field
        set(value) {field = value}

    var daysBeforExpiration: Int = 0
        get() = field
        set(value) {field = value}

    val isExpired : Boolean
        get() = daysBeforExpiration < 1

    val isExpiringSoon: Boolean
        get() = daysBeforExpiration < 7
}
