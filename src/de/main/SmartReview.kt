package de.main

class SmartReview(var stars : Int) : Review {

    var rating_descriptions = arrayOf("Schlechtes Produkt", "Mäßiges Produkt", "Durchschnittliches Produkt", "Brauchbares Produkt", "Gutes Produkt", "Exzellentes Produkt")

    var invalid_stars = "Nicht sinnvoll bewertet"

    override fun stars(): Int {
        return stars
    }

    override fun info(): String {
        if(stars in 0..10)
        {
            return rating_descriptions[stars+1]
        }
        else
        {
            return invalid_stars
        }
    }
}
