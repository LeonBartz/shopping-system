package de.main

class PlainReview(var stars: Int) : Review{
    override fun stars () : Int
    {
        return stars
    }

    override fun info() : String
    {
        return "Produkt mit $stars Sternen"
    }
}
