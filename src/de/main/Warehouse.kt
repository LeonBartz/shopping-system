package de.main

class Warehouse {

    val products = mutableListOf<Product>()

    var productDescriptions : String = ""

    val listOfProducts : String
        get() = listOfProductsToString()

    fun listOfProductsToString() : String
    {
        var list = ""
        for (product in products)
        {
            list += product.toString() + "\n"
        }
        return list
    }

    fun hasProduct( productName : String) :  Boolean
    {
        for (product in products)
        {
            if(product.name == productName){
                return true
            }
        }
        return false
    }

    fun getProductByName( productName: String): Product?
    {
        for (product in products)
        {
            if(product.name == productName){
                return product
            }
        }
        return null
    }

    fun fillWarehouse ( productName: String,
                        basePrice : Double,
                        description: String ,
                        chargeOnTop : Double = 0.0 ,
                        initialStockUnits : Int = 3 )
    {
        val product = DiscountProduct(productName, DiscountType.EVERYTING_OUT)
        product.basePrice = basePrice
        product.description = description
        product.salesPrice = basePrice + chargeOnTop
        product.addStock(StockUnit(initialStockUnits))
        products.add(product)
    }

}
