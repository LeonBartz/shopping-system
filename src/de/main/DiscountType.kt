package de.main

enum class DiscountType
{
    SUMMER {
        override fun discountFactor(): Double = 0.2
        override fun typeName() = "Sommerschluss"
    },

    MHD {
        override fun discountFactor(): Double = 0.1
        override fun typeName() = "Kurzes MHD"
    },

    EVERYTING_OUT {
        override fun discountFactor(): Double = 0.5
        override fun typeName() = "Alles muss raus"
    };

    open fun typeName() = ""
    open fun discountFactor() = 0.1
}
