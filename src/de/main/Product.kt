package de.main

import kotlin.math.round
import kotlin.collections.mutableListOf as mutableListOf
open class Product constructor (var name: String)
{

    var basePrice : Double = 0.0

    open var salesPrice : Double = 0.0

    var description : String = ""

    var reviews = mutableListOf<Review>()

    var stockUnits = mutableListOf<StockUnit>()

    fun profitPerItem() : Double
    {
        return salesPrice - basePrice
    }

    fun valueOfAllItems() : Double
    {
        return availableItems() * basePrice
    }

    fun salesValueOfAllItems() : Double
    {
        return availableItems() * salesPrice
    }

    fun availableItems() : Int
    {
        var count = 0
        for (stockUnit in stockUnits)
        {
            count += stockUnit.quantity
        }
        return count
    }

    override fun toString() : String
    {
        return "${availableItems()} x ${name} Preis: ${salesPrice.round(2)}($basePrice) Euro.\n ${description}\n"
    }

    fun addStock( items : StockUnit) : Boolean
    {
        return stockUnits.add(items)
    }

    fun addReview( review : Review) : Boolean
    {
        return reviews.add(review)
    }

    fun clearStock() {
        return stockUnits.clear()
    }

    fun isPreferredQuantityAvailable( preferredQuantity : Int) : Boolean
    {
        return availableItems() >= preferredQuantity
    }

    fun takeItems( preferrdQuantity : Int) : Int
    {
        var gotten = 0
        var missing = preferrdQuantity

        sortUnitsByExpiration()

        for (stockUnit in stockUnits)
        {
            if(missing == stockUnit.quantity)
            {
                missing = 0
                gotten += stockUnit.quantity
                stockUnit.quantity = 0
            }
            else if(missing > stockUnit.quantity)
            {
                missing -= stockUnit.quantity
                gotten += stockUnit.quantity
                stockUnit.quantity = 0
            }else if(missing < stockUnit.quantity)
            {
                gotten += missing
                stockUnit.quantity -= missing
                missing = 0
            }

            if(missing == 0)
            {
                return gotten
            }
        }

        return gotten
    }

    private fun sortUnitsByExpiration()
    {
        stockUnits.sortBy { stockUnit -> stockUnit.daysBeforExpiration }
    }

    fun Double.round(decimals: Int): Double {
        var multiplier = 1.0
        repeat(decimals) { multiplier *= 10 }
        return round(this * multiplier) / multiplier
    }
}
