package de.main

interface Review {

    fun stars () : Int

    fun info () : String
}
