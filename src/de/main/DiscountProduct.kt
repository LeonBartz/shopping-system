package de.main

class DiscountProduct(productName: String, var discount: DiscountType) : Product(productName)
{
    override var salesPrice : Double = 0.0
        get() = field * discount.discountFactor()
}
