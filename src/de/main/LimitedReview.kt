package de.main

class LimitedReview (var stars: Int, var info : String) : Review{
    override fun stars() : Int
    {
        if(stars < 0) return 0
        if(stars > 10) return 10
        return stars
    }

    override fun info(): String {
        return info
    }
}
